require 'rails_helper'
require 'pry-rails'

RSpec.describe EmailersController, type: :controller do

  describe 'EmailersController' do

    context 'send emails' do
      
      # Since in sandbox mode only emails can be sent to authorized recipients I have added d.k.khatri2012@gmail.com to authorized recipient list.
      # You can configure your own domain dns setting to enable sending emails to any email address
      it 'should send emails with a given subject, campaign_id, text to any email address ' do
        post :send_email,  to: 'd.k.khatri2012@gmail.com', subject: Faker::Lorem.sentence, campaign_id: 1, text: Faker::Lorem.paragraphs(1) 
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(body['message']['id']).to be_present
        expect(body['message']['message']).to be_present
        message = body['message']['message']
        expect(message).to eq('Queued. Thank you.')
      end

    end

    context 'detect if an email is present in a given suppression list' do
      
      # I have added tillu2014@gmail.com email address in unsubscibes list

      it 'detect shoud detect an email present in unsubscribed list' do
        get :detect_supression, email: 'tillu2014@gmail.com', type: 'unsubscribes' 
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(body['message']['address']).to be_present
        expect(body['message']['created_at']).to be_present
      end

      it 'should return a proper response if email is not present supression list' do
        get :detect_supression, email: 'tillu2014@gmail.com', type: 'bounces' 
        expect(response.status).to eq 404
        body = JSON.parse(response.body)
        expect(body['message']['message']).to be_present
        expect(body['message']['message']).to include("not found")
      end
    
    end
  end

   context 'Details of previously sent emails to an email address ' do

    it 'should get details of previously sent emails to an email address ' do
        get :get_details, email: 'd.k.khatri2012@gmail.com'
        expect(response.status).to eq 200
        body = JSON.parse(response.body)
        expect(body['message']).to be_present
        expect(body['message']['items']).to be_present
      end

   end

end

