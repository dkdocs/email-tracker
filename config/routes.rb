Rails.application.routes.draw do
   	post 'send_email', to: 'emailers#send_email'
    get  'detect_supression', to: 'emailers#detect_supression'
    get  'get_details', to: 'emailers#get_details'
    post 'open_event', to: 'emailers#open_event'
    post 'track_event', to: 'emailers#track_event'
end
