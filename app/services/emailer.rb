class Emailer
  require 'net/http'
  #change these according to your Account settings
  
  BASE_URL = 'https://api:key-61b88fb0321794b04cc3ca58f7d41567@api.mailgun.net/v3/sandboxc4865ee3c9ef4b20a020d5f4bbe0838c.mailgun.org'
  DOMAIN = 'Mailgun Sandbox <postmaster@sandboxc4865ee3c9ef4b20a020d5f4bbe0838c.mailgun.org>'
  
  def send_email(options)
    uri = URI("#{BASE_URL}/messages")
    req = Net::HTTP::Post.new(uri)
    req.basic_auth uri.user, uri.password
    req.set_form_data(:from => DOMAIN,
                      :to => options[:to],
                      :subject => options[:subject],
                      "o:campaign" => options[:campaign_id],
                      :text => options[:text],
                      :html =>  "<html>#{options[:text]}</html>",
                      "o:tracking-opens" => 'yes',
                      "o:tracking-clicks" => 'yes' )
    res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => true, :read_timeout => 5) do |http|
      http.request(req)
    end
    res
  rescue
    res = Object.new
    res.define_singleton_method(:body) { { message: "Service unreachable" }.to_json }
    res.define_singleton_method(:code) { 503 }
  end

  def get_details(email)
    uri = URI("#{BASE_URL}/events")
    params = { :recipient => email }
    uri.query = URI.encode_www_form(params)
    req = Net::HTTP::Get.new(uri)
    req.basic_auth uri.user, uri.password
    Net::HTTP.start(uri.hostname, uri.port, :use_ssl => true, :read_timeout => 5) { |http| http.request(req) }
  rescue
    res = { body: "Mailgun server unreachable", code: 503 }.to_json
  end

  def detect_supression(email, type)
      
      uri = URI("#{BASE_URL}/#{type}/#{email}")
      req = Net::HTTP::Get.new(uri)
      req.basic_auth uri.user, uri.password
      Net::HTTP.start(uri.hostname, uri.port, :use_ssl => true, :read_timeout => 5) { |http| http.request(req) }
      
  rescue
    res = Object.new
    res.define_singleton_method(:body) { { message: "Service unreachable" }.to_json }
    res.define_singleton_method(:code) { 503 }
  end



end