class EmailersController < ApplicationController
  
  before_action :valid_email?, except: :track_event
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  
  def send_email
      res = Emailer.new.send_email(params)
      render json: { message: JSON.parse(res.body) }, status: res.code
  end

  def detect_supression
    if  valid_type?(params[:type])
      res = Emailer.new.detect_supression(params[:email], params[:type])
      render json: { message: JSON.parse(res.body) }, status: res.code
    else
      render json: { message: "Improper parameters" }, status: :unprocessable_entity
    end
  end

  def get_details
    res = Emailer.new.get_details(params[:email])
    render json: { message: JSON.parse(res.body) }, status: res.code  
  end

  def track_event
    data = []
    params.each do | k,v|
      data << "#{k}=>#{v}" unless ['controller', 'action', 'body-plain'].include?(k)
    end
    cust_logger.info(data.join(','))
    render json: { message: "ok" }, status: :ok
  end

  private
  def cust_logger
     @@custom_logger ||= Logger.new("#{Rails.root}/log/tracker.log")
  end

  def valid_email?
    email = params[:email] || params[:to]
    render json: { message: "Improper parameters" }, status: :unprocessable_entity unless email.present? && (email =~ VALID_EMAIL_REGEX)
  end

  def valid_type?(type)
    %w(unsubscribes bounces complaints).include?(type)
  end


end
